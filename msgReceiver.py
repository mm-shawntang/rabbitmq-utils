#!/usr/bin/env python
import pika
from pika import exceptions
import datetime

queue_name = 'testqueue'


# node1 = pika.connection.ConnectionParameters('10.25.20.150', connection_attempts=1, retry_delay=0)
# node2 = pika.connection.ConnectionParameters('10.25.20.166', connection_attempts=1, retry_delay=0)
mqhost="amqps://rmqtest:rmqtest@b-bdc5d6be-16ec-4833-b025-5b37bf48750e.mq.ap-southeast-2.amazonaws.com:5671"
node1 = pika.URLParameters(mqhost)
node2 = pika.URLParameters(mqhost)
all_endpoints = [node1, node2]

connection = pika.BlockingConnection(all_endpoints)

channel = connection.channel()
method_frame, header_frame, body = channel.basic_get(queue_name)
if method_frame:
    print(method_frame, header_frame, body)
    channel.basic_ack(method_frame.delivery_tag)


channel.queue_declare(queue=queue_name, durable=True, exclusive=False, auto_delete=False)


def callback(ch, method, properties, body):
    channel.basic_ack(multiple=True)
    print(str(datetime.datetime.now()) + " [x] Received %r" % body + ' - ' + str(ch).split()[11])


channel.basic_consume(queue=queue_name,
                      auto_ack=False,
                      on_message_callback=callback)
print(' [*] Waiting for messages. To exit press CTRL+C')

try:
    channel.start_consuming()
except Exception as ex:
    print(str(ex.message))
    pass