RMQ="$1"
bin/runjava com.rabbitmq.perf.PerfTest \
-h amqp://guest:guest@$RMQ \
--id kill-rabbits-$RMQ \
--queue kill-rabbits-$RMQ \
--auto-delete false \
--type direct \
--flag persistent \
--producers 1 \
--rate 10 \
--producer-channel-count 1 \
--consumers 2 \
--consumer-rate 100 \
--consumer-channel-count 2 \
--qos 4 \
--json-body \
--nack \
--size 6124 \
--time 3600
