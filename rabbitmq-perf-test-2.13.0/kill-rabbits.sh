RMQ=$1
bin/runjava com.rabbitmq.perf.PerfTest \
-h amqp://guest:guest@$RMQ \
--id kill-rabbits \
--queue kill-rabbits \
--auto-delete false \
--type direct \
--flag persistent \
--producers 100 \
--rate 10000 \
--producer-channel-count 10 \
--consumers 300 \
--consumer-rate 30 \
--consumer-channel-count 4 \
--qos 4 \
--json-body \
--nack \
--size 6124 \
--time 3600 
