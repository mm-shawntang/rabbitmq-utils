#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pika
from pika import exceptions
import random
import string
import threading
import time

mq_exchange = 'test_exchange'
queue_name = 'testqueue'
routing_key = 'test.test'

# mqhost = "amqps://b-7c6db4c8-4b0c-4a34-bdb6-0cd5d2b5a9ea.mq.ap-southeast-2.amazonaws.com"
# mqport = "5671"
# user = "rmqtest"
# passwd = "rmqtest"
# node1 = pika.connection.ConnectionParameters(mqhost, port=mqport, connection_attempts=1, retry_delay=0)
# node2 = pika.connection.ConnectionParameters(mqhost, port=mqport, connection_attempts=1, retry_delay=0)

mqhost="amqps://rmqtest:rmqtest@b-bdc5d6be-16ec-4833-b025-5b37bf48750e.mq.ap-southeast-2.amazonaws.com:5671"
node1 = pika.URLParameters(mqhost)
node2 = pika.URLParameters(mqhost)
all_endpoints = [node1, node2]


def msg_send():
    message = 'msg-'
    for _ in range(2000):
        message += random.choice(string.ascii_letters + string.digits)

    try:
        connection = pika.BlockingConnection(all_endpoints)
        channel = connection.channel()
        # print(str(channel).split()[11])
        channel.exchange_declare(exchange=mq_exchange, exchange_type='topic', durable=True, auto_delete=False)
        channel.queue_declare(queue=queue_name, durable=True, exclusive=False, auto_delete=False)
        channel.queue_bind(exchange=mq_exchange, queue=queue_name, routing_key=routing_key)
        channel.basic_publish(exchange=mq_exchange,
                              routing_key=routing_key,
                              body=message,
                              properties=pika.BasicProperties(content_type='text/plain',
                                                              delivery_mode=2)
                              )
        # print(str(channel).split()[11])
        channel.close()
        connection.close()
    except Exception as e:
        print("-- exceptions err: ", str(e))




aRound = 100000
nRound = 10
for a in range(1, aRound):
    msg_send()
    time.sleep(2)
    # s = []
    # for n in range(1, nRound):
    #     s.append(threading.Thread(target=msg_send))
    # for th in s:
    #     th.start()
    print('round finished: ' + str(a))
