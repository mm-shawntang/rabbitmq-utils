#!/bin/bash

# Make sure there is a vhost for every ugapplications stack
# Make sure all users have access to all vhosts

RABBITMQCTL=/usr/sbin/rabbitmqctl

if [ $UID -ne 0 ]
then
        echo "Sorry, this must be run as root"
        exit
fi

# stacks=$(aws cloudformation list-stacks --region ap-southeast-2 --query "StackSummaries[*].[StackName, StackStatus]" --stack-status-filter CREATE_COMPLETE ROLLBACK_COMPLETE UPDATE_ROLLBACK_COMPLETE UPDATE_COMPLETE --output text | awk '{print $1}' | egrep "ugapplications$|ugapps$")

# for stack in $(echo $stacks)
for stack in "hz-ugapps"
do
        # Check it has a vhost
        vhost_count=$($RABBITMQCTL -q list_vhosts | egrep -c -w "^$stack$")

        if [ $vhost_count -eq 1 ]
        then
                # $RABBITMQCTL add_vhost $stack
		echo " - vhost: $stack"
        fi

        for user in $($RABBITMQCTL -q list_users | awk '{print $1}')
        do
                user_count=$($RABBITMQCTL -q list_permissions -p $stack | awk '{print $1}' | egrep -c -w "^$user$")

                if [ $user_count -eq 1 ]
                then
                        # $RABBITMQCTL set_permissions -p $stack $user ".*" ".*" ".*"
                        # echo ' - set_permissions -p' $stack $user '".*" ".*" ".*" ' 
			echo $user
                fi
        done

        # Add default ha policy to vhost
        # if ! $RABBITMQCTL list_policies -q -p $stack | grep -q ha-all ; then
                # $RABBITMQCTL set_policy -p $stack ha-all "" '{"ha-mode":"all"}'
		
        # fi

done
