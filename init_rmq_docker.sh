id=`sudo docker ps|grep rabbitmq|awk '{print $1}'`
sudo docker stop $id

sudo docker run --restart always -d -it --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.8.6-management
sleep 15

vhost="shw-ugapps"
./rabbitmqadmin declare vhost name=$vhost
# rabbitmqctl set_cluster_name rmq_shawn-test

userlist="
gatewayapi
inbound
apit
multipartinbound
sender
gatekeeper
managementdashboard
inboundecr
router
receiver
delayedretry
trafficpump
holdfilterbot
prioritisation
"

for u in $userlist
do
	./rabbitmqadmin declare user name=$u password=$u tags=""
	./rabbitmqadmin declare permission vhost=$vhost user=$u configure=.* write=.* read=.*
done
