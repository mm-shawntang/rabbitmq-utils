sudo iptables -A INPUT -p tcp --dport 5672 -j DROP
sudo iptables -A INPUT -p tcp --dport 25672 -j DROP
sudo iptables -A OUTPUT -p tcp --dport 5672 -j DROP
sudo iptables -A OUTPUT -p tcp --dport 25672 -j DROP
echo "...check rules"
sudo iptables -L
